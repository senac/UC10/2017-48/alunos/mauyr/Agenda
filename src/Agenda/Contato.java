
package Agenda;


public class Contato {
   private String nome;
   private String email;
   private String telefone;
   private String tipodeContato;

    public Contato(String nome, String email, String telefone, String tipodeContato) {
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        this.tipodeContato = tipodeContato;
    }

    public String getTipodeContato() {
        return tipodeContato;
    }

    public void setTipodeContato(String tipodeContato) {
        this.tipodeContato = tipodeContato;
    }

   
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
   
   
   
   
   
   
   
   
   
   
   
}
